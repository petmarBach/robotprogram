﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Microsoft.Kinect;
using System.Threading.Tasks;
using System.IO.Ports;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Collections;
using Microsoft.Speech.Recognition;
using Microsoft.Speech.AudioFormat;
using Microsoft.Kinect.Face;



namespace KinectAppCs
{

    

    /// <summary>
    /// New instance of KinectV class
    /// </summary>
    public class KinectV
    {

        USB usb = new USB();



        int closestBody;
        //int frames = 0;
        public static char emotion;
        public static float closeX;
        public static float closeY;


        //SerialPort mySerialPort = new SerialPort("COM3", 9600);
        //SerialPort mySerialPort = new SerialPort(Form1.comPort, 9600);
        /*    try
           {
               mySerialPort.Open();
           }
           catch (IOException)
           {
                   //Nothing atm.
           } */

        /// <summary>
        /// Declare active kinect sensor
        /// </summary>
        public KinectSensor kinectSensor = null;

        //Declare frame readers.
        /// <summary>
        /// Multi Source Framereader. Gets IR, color and body data.
        /// </summary>
        MultiSourceFrameReader msfReader = null;

        /// <summary>
        /// face frame sources
        /// </summary>
  //      FaceFrameSource faceSource = null;

        /// <summary>
        /// face frame readers
        /// </summary>
    //    FaceFrameReader faceReader = null;

        // create the bitmap to display
        private WriteableBitmap storeImg = null;



        /// <summary>
        /// "bodies": Array of bodies
        /// </summary>
        Body[] bodies = null;


        /// <summary>
        /// Method initialiseKinect. Is called from Form1.cs in own thread.
        /// </summary>
        public void initialiseKinect(object mySerialPort)
        {





            //Kinect Initialised
            kinectSensor = KinectSensor.GetDefault();
            //System.Threading.Thread.Sleep(1000);

            if (kinectSensor != null)
            {
                //Kinect ON
                kinectSensor.Open();

            }



            //Initialize the multisource framereader.

            //bodyFrameReader = kinectSensor.BodyFrameSource.OpenReader();
            this.msfReader = this.kinectSensor.OpenMultiSourceFrameReader(FrameSourceTypes.Body | FrameSourceTypes.Color);
            
            msfReader.MultiSourceFrameArrived += MsfReader_MultiSourceFrameArrived;

            // create colorframedescription
            FrameDescription colorFrameDescription = this.kinectSensor.ColorFrameSource.CreateFrameDescription(ColorImageFormat.Bgra);

            storeImg = new WriteableBitmap(colorFrameDescription.Width, colorFrameDescription.Height, 96.0, 96.0, PixelFormats.Bgr32, null);


            //this.kinectSensor.Open();



            //msfReader.PropertyChanged


            // Initialize the face source with the desired features
      /*      faceSource = new FaceFrameSource(kinectSensor, 0, FaceFrameFeatures.BoundingBoxInColorSpace |
                                                          FaceFrameFeatures.FaceEngagement |
                                                          FaceFrameFeatures.Glasses |
                                                          FaceFrameFeatures.Happy |
                                                          FaceFrameFeatures.LeftEyeClosed |
                                                          FaceFrameFeatures.MouthOpen |
                                                          FaceFrameFeatures.PointsInColorSpace |
                                                          FaceFrameFeatures.RightEyeClosed);

            faceReader = faceSource.OpenReader();
            faceReader.FrameArrived += FaceReader_FrameArrived;


            */
        }

        /// <summary>
        /// Handles the face frame data arriving
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        /*
        private void FaceReader_FrameArrived(object sender, FaceFrameArrivedEventArgs e)
        {

            using (var frame = e.FrameReference.AcquireFrame())
            {
                if ( frame != null)
                {

                    FaceFrameResult result = frame.FaceFrameResult;
                    if (result != null)
                    {

                        //HERE ARE ALL PROPERTIES RELATED TO FACE
                        var engaged = result.FaceProperties[FaceProperty.Engaged];
                        var happy = result.FaceProperties[FaceProperty.Happy];
                        var leftEyeClosed = result.FaceProperties[FaceProperty.LeftEyeClosed];
                        var lookingAway = result.FaceProperties[FaceProperty.LookingAway];
                        var mouthMoved = result.FaceProperties[FaceProperty.MouthMoved];
                        var mouthOpen = result.FaceProperties[FaceProperty.MouthOpen];
                        var rightEyeClosed = result.FaceProperties[FaceProperty.RightEyeClosed];
                        var wearingGlasses = result.FaceProperties[FaceProperty.WearingGlasses];

                        //Console.WriteLine(happy);
                    }
                }
            }
        }*/
        /// <summary>
        /// Handles all IR, body and color data arriving from the sensor.
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>

        private void MsfReader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            MultiSourceFrame msf = e.FrameReference.AcquireFrame();

            //Create reference to new image.
            var reference = e.FrameReference.AcquireFrame();
            
            // Open the color frame!
            using (var frame = reference.ColorFrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    // Do something with the frame...
                    
                }
            }

            

            // Open the skeleton/body frame!
            using (var frame = reference.BodyFrameReference.AcquireFrame())
            {
                //If there is a frame
                if (frame != null)
                {
                    
                    //Frame counter
                    //frames++;
                    //Console.WriteLine("Frame: " + frames);
                    
                    //Sets up array of 6 trackable bodies.
                    bodies = new Body[frame.BodyFrameSource.BodyCount];
                    
                    //trackedBodies = new Body[frame.BodyFrameSource.BodyCount];

                    //Gets new body data from frame. 
                    frame.GetAndRefreshBodyData(bodies);


                    //Declaration of variables before starting analysis.
                    float closest = float.MaxValue;
                    float close;
                    float oldClose = 0;
                    float x = 0;
                    float y = 0;
                    float z = 0;

                    // foreach (Body body in bodies)
                    for (int i = 0; i < frame.BodyFrameSource.BodyCount; i++)
                    {
                        if (bodies[i].IsTracked)
                        {
                            //Console.WriteLine("BODY FOUND!!!!!!!");
           //                 faceSource.TrackingId = bodies[i].TrackingId;

                            /*
                            IF ONLY 6 PEOPLE CAN BE TRACKED AT ONE TIME, MAYBE SET UP SOME WAY TO HANDLE IT?

                            //05.05 probably not possible, the kinect can't remember specific users.

                            //Put bodies in trackable bodies.
                            trackedBodies[i] = bodies[i];

                            */

                            /*
                            DATA COLLECTION 
                            IN THIS PART OF THE PROGRAM, ALL INFORMATION ON THE SKELETONS IN THE FRAME IS AQUIRED. 

                            */

                            //GETS ALL THE JOINTS OF THE BODY 

                            Joint rightHand = bodies[i].Joints[JointType.HandRight];
                            Joint leftHand = bodies[i].Joints[JointType.HandLeft];
                            Joint head = bodies[i].Joints[JointType.Head];
                            Joint ankleLeft = bodies[i].Joints[JointType.AnkleLeft];
                            Joint ankleRight = bodies[i].Joints[JointType.AnkleRight];
                            Joint elbowLeft = bodies[i].Joints[JointType.ElbowLeft];
                            Joint elbowRight = bodies[i].Joints[JointType.ElbowRight];
                            Joint footLeft = bodies[i].Joints[JointType.FootLeft];
                            Joint footRight = bodies[i].Joints[JointType.FootRight];
                            Joint handTipLeft = bodies[i].Joints[JointType.HandTipLeft];
                            Joint handTipRight = bodies[i].Joints[JointType.HandTipRight];
                            Joint hipLeft = bodies[i].Joints[JointType.HipLeft];
                            Joint hipRight = bodies[i].Joints[JointType.HipRight];
                            Joint kneeLeft = bodies[i].Joints[JointType.KneeLeft];
                            Joint kneeRight = bodies[i].Joints[JointType.KneeRight];
                            Joint neck = bodies[i].Joints[JointType.Neck];
                            Joint shoulderLeft = bodies[i].Joints[JointType.ShoulderLeft];
                            Joint shoulderRight = bodies[i].Joints[JointType.ShoulderRight];
                            Joint spinseBase = bodies[i].Joints[JointType.SpineBase];
                            Joint spineMid = bodies[i].Joints[JointType.SpineMid];
                            Joint spineShoulder = bodies[i].Joints[JointType.SpineShoulder];
                            Joint thumbLeft = bodies[i].Joints[JointType.ThumbLeft];
                            Joint thumbRight = bodies[i].Joints[JointType.ThumbRight];
                            Joint wristLeft = bodies[i].Joints[JointType.WristLeft];
                            Joint wristRight = bodies[i].Joints[JointType.WristRight];

                          


                            //Coordinates of a joint
                            //HEAD
                            x = head.Position.X;
                            y = head.Position.Y;
                            z = head.Position.Z;


                            //HEAD

                            //Other joints --- 

                            //find index of body that is closest to the camera.
                          /*  close = z;
                            if (close < closest && oldClose-close >= 0.2 )
                            {
                                closest = close;
                                closestBody = i;

                            }
                            else if(close < closest && oldClose-close <= -0.2)
                            {
                                closest = close;
                                closestBody = i;

                            } */
                            

                            
                            close = z;
                            if (close < closest)
                            {
                                closest = close;
                                closestBody = i;
                            }
                              

                            switch (bodies[i].HandRightState)
                            {
                                case HandState.Open:

                                    Console.WriteLine("OPEN");
                                    break;

                                case HandState.Closed:
                                    Console.WriteLine("CLOSED");
                                    break;


                            }



                        } //End if tracked
                        else
                        {
                            
                            //mySerialPort.Write("x");
                        }
                    } // End for body in bodies
                    
                    
                    //LOGIC FOR CHOOSING WHO TO FOCUS ON.

                    /*
                    if( following closest = true)
                    {
                        x coordinate / direction = bodies[closest]

                    }




                    */



                    /*
                    
                    This part concerns all results from the frame analysis. These include:
                    
                    - 'closestBody', which is an integer representing the body closest to the camera.
                    - "other integers, maybe furthest away? or if a body makes a specific move.
                    
                    
                    The resulting integer should represent a "focus", which is the person the robot should 
                    concentrate on.

                    */
                    int focus;

                    Joint closestHeadX = bodies[closestBody].Joints[JointType.Head];
                    Joint closestHeadY = bodies[closestBody].Joints[JointType.Head];
                    closeX = closestHeadX.Position.X;
                    closeY = closestHeadY.Position.Y;
                    
                    oldClose = closeX;

                    /*

                    CODE HERE COMES IN EFFECT AFTER ALL BODIES HAVE BEEN ANALYSED.

                    */

                    //find closest body. Returns the closest.
                    FindClosestBody(frame);



                    //Console.WriteLine(Webserver.direction);
                    //Mode.mode = "1";

                    switch (Mode.mode)
                    {
                        case "1":  
                            //Console.WriteLine("CAMERA MODE");
                           // Console.WriteLine("closeX = " + closeX);
                            //mySerialPort.Open();
                            //cameraRuntime(closeX, mySerialPort);
                            
                            
                            //if(closeX > 0.15)
                            //{
                            //    Console.WriteLine("Rotate RIGHT");
                           // }
                            //if (closeX < -0.15)
                           // {
                             //   Console.WriteLine("Rotate LEFT");
                            //}



                            //mySerialPort.Close();
                            break;

                        case "2":
                            //Console.WriteLine("APP MODE");
                            //appRuntime(Webserver.direction, mySerialPort);
                            //Console.WriteLine(Webserver.direction);

                            break;
                        case "3":
                            //Console.WriteLine("SPEECH MODE");
                            //appRuntime(Speech.direction, mySerialPort);
                            //Console.WriteLine(Speech.direction);
                            break;

                        case "4":
                            //standBy();
                                break;

                        default:
                            //Console.WriteLine("defaulted");
                            break;

                    }


                } //if frame != null


            } //Using var frame arrived


        } //End of frame arrived

       // cameraRuntime(closeX, mySerialPort);
        //Close the serial port 
        //mySerialPort.Close();

        /// <summary>
        /// listens for console cancel event. Closes the kinectsensor and multisourceframereader.
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        
        private void OnProcessExit(object sender, ConsoleCancelEventArgs e)
        {
            if (this.msfReader != null)
            {
                this.msfReader.Dispose();
                this.msfReader = null;

            }

            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }


        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="bodyFrame"></param>
        /// <returns></returns>

        private static Body FindClosestBody(BodyFrame frame)
        {
            Body result = null;
            double closestBodyDistance = double.MaxValue;

            Body[] bodies = new Body[frame.BodyCount];
            frame.GetAndRefreshBodyData(bodies);

            foreach (var body in bodies)
            {
                if (body.IsTracked)
                {
                    var currentLocation = body.Joints[JointType.SpineBase].Position;

                    var currentDistance = VectorLength(currentLocation);

                    if (result == null || currentDistance < closestBodyDistance)
                    {
                        result = body;
                        closestBodyDistance = currentDistance;
                    }
                }
            }

            return result;
        }

        private static double VectorLength(CameraSpacePoint point)
        {
            var result = Math.Pow(point.X, 2) + Math.Pow(point.Y, 2) + Math.Pow(point.Z, 2);

            result = Math.Sqrt(result);

            return result;
        }



        private void onMFSR(object sender, PropertyChangedEventArgs e)
        {
            //Console.WriteLine("RORKORKEOKREO");
        }
        

        

        /*
        The following signals have been made on the microcontroller:

        Engine1 Left = 7
        Engine1 Right = 9
        
        Engine2 Left = 4
        Engine2 Right = 6
        
        Engine3 Left = 1
        Engine3 Right = 3

       
        Other commands that can be used:

        Stop all engines = 'x'
        
        Disable all engines = 'z' 

        */

        /*

        IN THE FUTURE DATA COULD PERHAPS BE SENT AS A BUFFER INSTEAD OF A SINGLE LETTER.

        i.e. myserialport.write("number of steps", "command").


        */

    }
        
    

} //End public class KinectV

