﻿using System;
using Microsoft.Kinect;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace KinectAppCs
{
    public class Program
    {

        /// <summary>
        /// Contains main in the console application. Starts form1.
        /// </summary>
        /// <param name="args"></param>
        [STAThread]
        static void Main(string[] args)
        {
            

            Application.Run(new Form1());

        }
    } //End Class Program

}
