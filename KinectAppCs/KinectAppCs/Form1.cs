﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using Microsoft.Speech.Recognition;
using Microsoft.Speech.AudioFormat;
using System.IO;
using System.IO.Ports;
using System.Management;

namespace KinectAppCs
{
    /// <summary>
    /// Initialize new partial class Form1
    /// </summary>
    public partial class Form1 : Form
    {
        KinectV kinect = new KinectV();
        //Webserver ws = new Webserver();
        Speech s = new Speech();
        Mode mode = new Mode();
        USB usb = new USB();
        //SerialConnection usb = new SerialConnection();

        public static Thread t1, t2, t3, t4, t5;

        public static System.Timers.Timer timer2 = new System.Timers.Timer();
        public static int blink;
        public static string comPort;


        /// <summary>
        /// Intialize class form1. Starts threads from all methods.
        /// </summary>
        public Form1()
        {
            
            string[] ports = SerialPort.GetPortNames();

            foreach(string port in ports)
            {
                //Console.WriteLine(port);
            }
            comPort = ports[0]; 
/*
            SerialPort mySerialPort = new SerialPort(comPort, 9600);
               try
               {
                   mySerialPort.Open();
               }
               catch (IOException)
               {
                       //Nothing atm.
               } */


            InitializeComponent();
            /*
                Thread t2 = new Thread(kinect.initialiseKinect);
                Thread t1 = new Thread(s.speechRecognizer);
                Thread t3 = new Thread(ws.myServer);
                Thread t4 = new Thread(mode.setMode);
                */
            t1 = new Thread(s.speechRecognizer);
            t2 = new Thread(kinect.initialiseKinect);
            //t3 = new Thread(ws.myServer);
            t4 = new Thread(mode.setMode);
            t5 = new Thread(usb.sendData);


            t2.Start();
            t1.Start();
           // t3.Start();
            t4.Start();
            t5.Start();


            
            //DEBUGGING

            //MODES:
            // 1 IS CAMERA MODE
            // 2 IS APP / REMOTE CONTROL MODE
            // 3 IS VOICE CONTROL
            
            //Console.WriteLine("Type 1 for camera, 2 for ws, 3 for voice");
        }

        /// <summary>
        /// Consists of visual output to main monitor.
        /// Controls how the eye works.
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">evemt arguments</param>
        public void Form1_Load(object sender, EventArgs e)
        {
            /*FIX FULL SCREEN AND CENTER OF SCREEN */
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
            pictureBox1.Dock = DockStyle.Fill;
            pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox1.Show();
            //pictureBox1.Show();

            //Pick a random number between 2 and 8
            
            Random rand = new Random();
            blink = rand.Next(2, 8);

            Console.WriteLine(blink);

           
            timer2.Enabled = true;
            timer2.Interval = blink*1000;
            timer2.Start();
            timer2.Elapsed += Timer2_Elapsed;

        }
        /// <summary>
        /// Timer to make the eye "blink"
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>

        private void Timer2_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            
            
            for (int i = 1; i <= 13; i++)
            {
                pictureBox1.ImageLocation = "c:/Users/Markus/Documents/robotprogram/KinectAppCs/KinectAppCs/Eye/" + i + ".jpg";
                System.Threading.Thread.Sleep(10);
            }

            for (int i = 13; i >= 1; i--)
            {
                pictureBox1.ImageLocation = "c:/Users/Markus/Documents/robotprogram/KinectAppCs/KinectAppCs/Eye/" + i + ".jpg";
                System.Threading.Thread.Sleep(10);
            }
        }
        /*
        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        } */



    }
}
