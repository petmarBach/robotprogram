﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Net;
using System.Web;
using System.IO;

namespace KinectAppCs
{
    /// <summary>
    /// Initialize class webserver
    /// </summary>
    public class Webserver
    {
        public static string direction;

        /// <summary>
        /// Contains a httpListener that can give the robot commands using a html webform
        /// </summary>
        public void myServer()
        {
            
            HttpListener listener = new HttpListener();
            listener.Prefixes.Add("Http://localhost:9050/");
            //listener.Prefixes.Add("Http://158.36.195.97:9050/");

            //string dir = @"C:\Users\robot\Documents\rep\KinectAppCs\KinectAppCs\www\";
            string dir = @"C:\Users\Markus\Documents\robotprogram\KinectAppCs\KinectAppCs\www\";
            Directory.SetCurrentDirectory(dir);

            Console.WriteLine("Listening..");
            listener.Start();

            try
            {
                while (listener.IsListening)
                {
                    var context = listener.GetContext();
                    try
                    {
                        string method = context.Request.HttpMethod;
                        Console.WriteLine("Method = " + method);
                        System.IO.Stream body = context.Request.InputStream;
                        System.Text.Encoding encoding = context.Request.ContentEncoding;
                        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                        Uri unparsedUri = context.Request.Url;
                        Console.WriteLine(unparsedUri);
                        Console.WriteLine("ROOT DIRECTORY:");
                        Console.WriteLine(Directory.GetCurrentDirectory());

                        
                            

                        if(Path.HasExtension(unparsedUri.AbsoluteUri))
                        {
                            //Console.WriteLine("IS FILE YES");
                            //string fileName = System.IO.Path.GetFileName(unparsedUri.LocalPath);
                            string fileName = Path.GetFileName(unparsedUri.AbsolutePath);
                            Console.WriteLine(fileName);
                            //Console.WriteLine("BEFORE STARTING FILESTREAM");

                            var response = context.Response;
                            using (FileStream fs = File.OpenRead(dir+fileName))
                            {
                                Console.WriteLine("STARTING FILESTREAM");
                                response.ContentLength64 = fs.Length;
                                response.SendChunked = false;
                                //response.ContentType = System.Net.Mime.MediaTypeNames.Application.Octet;
                                response.ContentType = "text/html";
                                //response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                                byte[] buffer = new byte[64 * 1024];
                                int read;
                                using (BinaryWriter bw = new BinaryWriter(response.OutputStream))
                                {
                                    while ((read = fs.Read(buffer, 0, buffer.Length)) > 0)
                                    {
                                        bw.Write(buffer, 0, read);
                                        bw.Flush();
                                    }
                                    bw.Close();
                                }
                                response.StatusCode = (int)HttpStatusCode.OK;
                                response.StatusDescription = "OK";
                                response.OutputStream.Close();

                            }

                        }
                       
                        string queryString = unparsedUri.Query;
                        var queryParameters = HttpUtility.ParseQueryString(queryString);

                        direction = queryParameters["dir"];
                        //Sleep for a few seconds giving app some time 
                        System.Threading.Thread.Sleep(1000);
                        //Reset direction or else it will run again.
                        direction = "x";
                        Console.WriteLine(direction);

                        reader.Close();

                    }
                    catch { }
                }

            }
            catch { }
  
            listener.Stop();
        }






    }
}
