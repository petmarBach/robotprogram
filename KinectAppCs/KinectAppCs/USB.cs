﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Ports;



namespace KinectAppCs
{
    public partial class USB
    {
        //Global variables
        //public string 
        public static bool rest = false;


        //SerialPort mySerialPort = new SerialPort(Form1.comPort, 9600);


        public void sendData()
        {

           SerialPort mySerialPort = new SerialPort(Form1.comPort, 9600);

           try
           {
                mySerialPort.Open();
           }
           catch (IOException)
           {
                   //Nothing atm.
           } 
            while (true)
            {
                //Console.WriteLine("USB IS RUNNING WHILE TRUE");
                //check for mode and value of variable


                if (rest == true)
                {
                    mySerialPort.Write("z");
                    rest = false;
                }


                
                switch (Mode.mode)
                {
                    case "1":
                        //Console.WriteLine("CAMERA MODE");
                        //mySerialPort.Open();
                        //cameraRuntime(closeX, mySerialPort);
                        //cameraRuntime(KinectV.)
                        cameraRuntime(KinectV.closeX, KinectV.closeY, mySerialPort);
                        //mySerialPort.Close();


                        break;
                        
                    case "2":
                        //Console.WriteLine("APP MODE");
                        // appRuntime(Webserver.direction, mySerialPort);
                        //Console.WriteLine(Webserver.direction);

                        string control;

                        control = Console.ReadLine();

                        switch (control)
                        {
                            case "7":
                                mySerialPort.Write("7");
                                break;
                            case "9":
                                mySerialPort.Write("9");
                                break;
                            case "4":
                                mySerialPort.Write("4");
                                break;
                            case "6":
                                mySerialPort.Write("6");
                                break;
                            case "1":
                                mySerialPort.Write("1");
                                break;
                            case "3":
                                mySerialPort.Write("3");
                                break;
                            case "x":
                                mySerialPort.Write("x");
                                break;
                            case "z":
                                mySerialPort.Write("z");
                                break;

                            default: Mode.mode = "1";
                                break;
                        }

                        break;
                    case "3":
                        //Console.WriteLine("SPEECH MODE");
                        //appRuntime(Speech.direction, mySerialPort);
                        //Console.WriteLine(Speech.direction);
                        break;

                    case "4":
                        //standBy();
                        mySerialPort.Write("z");
                        break;

                    default:
                        //Console.WriteLine("defaulted");
                        break;

                }
            }
        }

                    /*
        The following signals have been made on the microcontroller:

        Engine1 Left = 7
        Engine1 Right = 9
        
        Engine2 Left = 4
        Engine2 Right = 6
        
        Engine3 Left = 1
        Engine3 Right = 3

       
        Other commands that can be used:

        Stop all engines = 'x'
        
        Disable all engines = 'z' 

        */

            /*

            IN THE FUTURE DATA COULD PERHAPS BE SENT AS A BUFFER INSTEAD OF A SINGLE LETTER.

            i.e. myserialport.write("number of steps", "command").

            */

            /// <summary>
            /// Method created to send data to microcontroller
            /// </summary>
            /// <param name="x">chosen coordinate to coordinate</param>
            /// <param name="mySerialPort">usb connection</param>
        public void stop(float x, SerialPort mySerialPort)
        {
            mySerialPort.Write("x");
        }
        /// <summary>
        /// Method created to send data to microcontroller
        /// </summary>
        /// <param name="x">chosen coordinate to coordinate</param>
        /// <param name="mySerialPort">usb connection</param>

        public void left(float x, SerialPort mySerialPort)
        {
            mySerialPort.Write("7");

        }
        /// <summary>
        /// Method created to send data to microcontroller
        /// </summary>
        /// <param name="x">chosen coordinate to coordinate</param>
        /// <param name="mySerialPort">usb connection</param>
        public void right(float x, SerialPort mySerialPort)
        {
            mySerialPort.Write("9");

        }


        public void up(float y, SerialPort mySerialPort)
        {
            //CASE FOR MAKING THE ROBOT LOOK UPWARD
            mySerialPort.Write("4");

        }
        public void down(float y, SerialPort mySerialPort)
        {
            //CASE FOR MAKING THE ROBOT LOOK DOWNWARD
            mySerialPort.Write("6");

        }

        /// <summary>
        /// Logic that decided which method will be run when the motors follow users using the camera.
        /// </summary>
        /// <param name="x">coordinate of user in x direction</param>
        /// <param name="y">coordinate of user height</param>
        /// <param name="mySerialPort">usb connection</param>


        //THIS IS SUPPOSED TO RUN WHEN CAMERA MAKES DECISIONS.
        public void cameraRuntime(float x, float y, SerialPort mySerialPort)
        {
            
            //TARGET LOCATING


            //if the target is higher and to the right 
            //rotate upward
            if (x > 0 && y > 0)
            {
                //if the target is to the right and the same height.
                if (x > 0.15)
                {
                    left(x, mySerialPort);
                }

                //if the target is to the right and above the camera,
                //do a 15 degree preset rotation upward.

                else if (x > 0.15 && y > 0.15)
                {
                    //Code for making the robot rotate upward....

                }

                else
                {
                    stop(x, mySerialPort);
                }
            }

            //if the target is lower and to the right 
            //rotate downward 
            else if (x > 0 && y < 0)
            {

                //if the target is to the right and the same height.
                if (x > 0.15)
                {
                    left(x, mySerialPort);
                }

                //if the target is to the right and above the camera,
                //do a 15 degree preset rotation downward.

                else if (x > 0.15 && y < -0.15)
                {
                    //Code for making the robot rotate downward...
                }
            }
            //if the target is higher and to the left
            //rotate upward
            else if (x < 0 && y > 0)
            {
                //if the target is to the left and the same height.
                if (x < -0.15)
                {
                    right(x, mySerialPort);
                }

                //if the target is to the left and above ,
                //do a 15 degree preset rotation upward.

                else if (x > 0.15 && y > 0.15)
                {
                    //Code for making the robot rotate downward....

                }
                else
                {
                    stop(x, mySerialPort);
                }


            }
            //if the target is lower and to the left
            //rotate downward
            else if (x < 0 && y < 0)
            {
                if (x < -0.15)
                {
                    right(x, mySerialPort);
                }
                else if (x < -0.15 && y < -0.15)
                {

                }


            }

            else if (x < -0.10)
            {
                if (x < 0)
                {
                    left(x, mySerialPort);
                }
                else
                {
                    stop(x, mySerialPort);
                }
            }
            else
                stop(x, mySerialPort);
           /* else
            {
                if (y > 0.10)
                {
                    up(y, mySerialPort);
                }
                else if (y < -0.10)
                {
                    down(y, mySerialPort);
                }
            } */

            /*
                        if (x < -0.15)
                        {
                            Console.WriteLine(x);
                            //mySerialPort.Write("0");

                            while (x < -0.15)
                            {
                                right(x, mySerialPort);
                            }

                        }

                        else if (x > 0.15)
                        {
                            Console.WriteLine(x);

                            while (x > 0.15)
                            {
                                left(x, mySerialPort);
                            }
                        }
                        else
                        {
                            stop(x, mySerialPort);
                        }
                            */




        } //End Camera Runtime


        /// <summary>
        /// Code running when speech or webserver decides the direction
        /// </summary>
        /// <param name="dir">string specifying which section while move what way</param>
        /// <param name="mySerialPort">usb connection</param>


        public void appRuntime(string dir, SerialPort mySerialPort)
        {

            //high left / right
            //middle left / right
            //lower left / right
            // x locks the motors. z puts them to sleep 

            Console.WriteLine(dir);
            if (dir == "Hleft")
            {
                mySerialPort.Write("7");
            }
            else if (dir == "Hright")
            {
                mySerialPort.Write("9");
            }

            else if (dir == "Mleft")
            {
                mySerialPort.Write("4");
            }

            else if (dir == "Mright")
            {
                mySerialPort.Write("6");
            }

            else if (dir == "Bleft")
            {
                mySerialPort.Write("1");
            }

            else if (dir == "Bright")
            {
                mySerialPort.Write("3");
            }
            //LOCK ENGINES 
            else
            {
                mySerialPort.Write("x");
            }
        }
    }

    
}
