﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using Microsoft.Kinect;
using Microsoft.Speech.AudioFormat;
using Microsoft.Speech.Recognition;
using System.Speech.Synthesis;
using System.Xml;
using System.Net;
using System.Web;
using System.IO;
using System.Globalization;
using DDay.iCal;


namespace KinectAppCs
{
    class Speech
    {

        public static string direction ;
        public bool listening = false;
        public bool reply = false;
        string cmd;

        //
        //This part holds code concerning audio!!
        //
        SpeechSynthesizer synth = new SpeechSynthesizer();
                

        KinectSensor kinectSensor = null;

        

        //Stream for 32 to 16 bit conversion
        KinectA convertStream = null;

        //Speech recognition engine.
        SpeechRecognitionEngine speechEngine = null;




        private static readonly Dictionary<Direction, Direction> rotateRight = new Dictionary<Direction, Direction>
        {
            {Direction.Left, Direction.Right},
            {Direction.Still, Direction.Right}
        };

        private static readonly Dictionary<Direction, Direction> rotateLeft = new Dictionary<Direction, Direction>
        {
            {Direction.Right, Direction.Left},
            {Direction.Still, Direction.Left}
        };


        private enum Direction
        {
            //Rotate robot to the left
            Left,
            //Rotate robot the the right
            Right,
            //Still
            Still,
        }

        Direction currentDirection = Direction.Still;
        /// <summary>
        /// Creates a recognition engine
        /// </summary>
        /// <returns></returns>

        private static RecognizerInfo TryGetKinectRecognizer()
        {
            IEnumerable<RecognizerInfo> recognizers;
            try
            {
                recognizers = SpeechRecognitionEngine.InstalledRecognizers();
            }
            catch
            {
                return null;
            }
            foreach (RecognizerInfo recognizer in recognizers)
            {
                string value;
                recognizer.AdditionalInfo.TryGetValue("Kinect", out value);
                if ("True".Equals(value, StringComparison.OrdinalIgnoreCase) && "en-US".Equals(recognizer.Culture.Name, StringComparison.OrdinalIgnoreCase))
                {
                    return recognizer;
                }
            } //end foreach
            return null;
        }

        /// <summary>
        /// Initialize class speechRecognizer
        /// </summary>

        public void speechRecognizer()
        {
            /*
            foreach (InstalledVoice voice in synth.GetInstalledVoices())
            {
                VoiceInfo info = voice.VoiceInfo;
                string audiof = "";
                foreach (System.Speech.AudioFormat.SpeechAudioFormatInfo fmt in synth.Voice.SupportedAudioFormats)
                {
                    audiof += String.Format("{0}\n",
                       fmt.EncodingFormat.ToString());

                }

                Console.WriteLine(" Name:          " + info.Name);
                Console.WriteLine(" Culture:       " + info.Culture);
                Console.WriteLine(" Age:           " + info.Age);
                Console.WriteLine(" Gender:        " + info.Gender);
                Console.WriteLine(" Description:   " + info.Description);
                Console.WriteLine(" ID:            " + info.Id);
                Console.WriteLine(" Enabled:       " + voice.Enabled);


            }
            */
            kinectSensor = KinectSensor.GetDefault();

       

                if (this.kinectSensor != null)
                {
                    this.kinectSensor.Open();
                    // grab the audio stream
                    IReadOnlyList<AudioBeam> audioBeamList = this.kinectSensor.AudioSource.AudioBeams;
                    System.IO.Stream audioStream = audioBeamList[0].OpenInputStream();

                    // create the convert stream
                    this.convertStream = new KinectA(audioStream);
                }


            

           // RecognizerInfo rec = 
            RecognizerInfo ri = TryGetKinectRecognizer();

            if (null != ri)
            {
                //Console.WriteLine("SPEECH WORKS");

                this.speechEngine = new SpeechRecognitionEngine(ri);

                var directions = new Choices();

                //Activation cases
                /*
                directions.Add(new SemanticResultValue("roto", "ROTO"));
                directions.Add(new SemanticResultValue("hey", "ROTO"));
                directions.Add(new SemanticResultValue("hi", "ROTO"));
                directions.Add(new SemanticResultValue("hello", "ROTO"));
                directions.Add(new SemanticResultValue("afternoon", "ROTO"));
                directions.Add(new SemanticResultValue("hi roto", "ROTO"));
                directions.Add(new SemanticResultValue("Good evening", "ROTO"));
                directions.Add(new SemanticResultValue("Good morning", "ROTO"));
                directions.Add(new SemanticResultValue("Good afternoon", "ROTO"));
                */
                //directions.Add(new SemanticResultValue("", "ROTO"));
                directions.Add(new SemanticResultValue("Start listening", "ROTO"));



                //Other verbal cases
                directions.Add(new SemanticResultValue("right", "RIGHT"));
                directions.Add(new SemanticResultValue("left", "LEFT"));
                directions.Add(new SemanticResultValue("stop", "STOP"));
                directions.Add(new SemanticResultValue("camera", "CAMERA"));
                directions.Add(new SemanticResultValue("roto, rest", "REST"));
                directions.Add(new SemanticResultValue("activate rest", "REST"));

                
                directions.Add(new SemanticResultValue("hi", "HELLO"));
                directions.Add(new SemanticResultValue("hello", "HELLO"));
                directions.Add(new SemanticResultValue("howdy", "HELLO"));
                directions.Add(new SemanticResultValue("hello?", "HELLO"));
                


                //Talk about roto

                //too similar to how are you
                //directions.Add(new SemanticResultValue("who are you", "MYSELF"));
                directions.Add(new SemanticResultValue("tell me about yourself", "MYSELF"));

                directions.Add(new SemanticResultValue("how are you?", "RMYSELF"));
                directions.Add(new SemanticResultValue("how are you", "RMYSELF"));

                //directions.Add(new SemanticResultValue("tell me", "MYSELF"));
                //directions.Add(new SemanticResultValue("about yourself", "MYSELF"));

                //Talk about random things
                directions.Add(new SemanticResultValue("how is the weather", "WEATHER"));
                //directions.Add(new SemanticResultValue("the weather", "WEATHER"));
                //directions.Add(new SemanticResultValue("about yourself", "MYSELF"));
                // directions.Add(new SemanticResultValue("about yourself", "MYSELF"));

                //calendar
                directions.Add(new SemanticResultValue("calendar", "CALENDAR"));

                //HSN
                directions.Add(new SemanticResultValue("Tell me about the school", "HSN"));
                directions.Add(new SemanticResultValue("Tell me about HSN", "HSN"));
                //directions.Add(new SemanticResultValue("how is the weather", "WEATHER"));

                //Jokes
                directions.Add(new SemanticResultValue("Tell me a joke", "JOKE"));
                directions.Add(new SemanticResultValue("Tell me another joke", "JOKE"));
                directions.Add(new SemanticResultValue("joke", "JOKE"));








                //directions.Add(new SemanticResultValue("who are you", "MYSELF"));



                //directions.Add(new SemanticResultValue(""))
                var gb = new GrammarBuilder { Culture = ri.Culture };
                gb.Append(directions);
                var g = new Grammar(gb);
                Console.WriteLine("grammar built");
                this.speechEngine.LoadGrammar(g);

                //
                //
                this.speechEngine.SpeechDetected += SpeechEngine_SpeechDetected;
                this.speechEngine.SpeechRecognized += SpeechEngine_SpeechRecognized;
                this.speechEngine.SpeechRecognitionRejected += SpeechEngine_SpeechRecognitionRejected;
                this.speechEngine.SpeechHypothesized += SpeechEngine_SpeechHypothesized;

                
                //
                //convertstream
                this.convertStream.SpeechActive = true;

                //For long sessions. Not recognized!
                //speechEngine.UpdateRecognizerSetting("AdaptionOn", 0);
                //
                //
                speechEngine.SetInputToAudioStream(
                    this.convertStream, new SpeechAudioFormatInfo(EncodingFormat.Pcm, 16000, 16, 1, 32000, 2, null));
                this.speechEngine.RecognizeAsync(RecognizeMode.Multiple);




            }

            else
            {
                Console.WriteLine("No speech recognizer her ;'(");
                //ri = TryGetKinectRecognizer();
            }

            
         }
        /// <summary>
        /// Event fires when speech is hypthesized but not clearly understood
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>

        private void SpeechEngine_SpeechHypothesized(object sender, SpeechHypothesizedEventArgs e)
        {
            //throw new NotImplementedException();
            //Console.WriteLine("wat");
        }


        /// <summary>
        /// Event fires when speech is detected but not understood
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void SpeechEngine_SpeechDetected(object sender, SpeechDetectedEventArgs e)
        {
            //throw new NotImplementedException();
            //Console.WriteLine("Hello?");
    
        }

        /// <summary>
        /// Event fires when speech is recognized.
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void SpeechEngine_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            Console.WriteLine("SPEECH RECOGNIZED");

            bool rWeather = false;
            Console.WriteLine(e.Result.Text);

            //Sets the type of voice
            synth.SelectVoiceByHints(VoiceGender.Male, VoiceAge.Adult);

            //CONFIDENCE THRESHOLD. HOW SURE THE KINECT IS ABOUT THE RESULT.
            const double ConfidenceThreshold = 0.6;

            Console.WriteLine(e.Result.Confidence);

            //IF RESULT CONFIDENCE IS GREATER THAN THE THRESHOLD
            if (e.Result.Confidence >= ConfidenceThreshold)
            {
                //check boolean if listening is true
                //if (listening == true || reply == false)
                

                    switch (e.Result.Semantics.Value.ToString())
                    {

                        //Cases for normal conversation
                        case "HELLO":
                        //synth.SpeakAsync("Greetings! How do you do?");
                        synth.SpeakAsync("Hello!");
                        break;

                        case "MYSELF":
                            synth.SpeakAsync("I am row-toe the robot");
                            synth.SpeakAsync("I was built by four engineering students as a bachelors project at the university college of south-east Norway. ");
                            break;

                        case "RMYSELF":
                            synth.SpeakAsync("I am fine, how are you?");
                            bool reply = true;
                            break;

                    case "CALENDAR":

                        Uri uri = new Uri("https://no.timeedit.net/web/hsn/db1/publikk/s.ics?i=6Q56670n5035QQ11Z54Y5Z5ZyQ6301");
                        IICalendarCollection calendars = iCalendar.LoadFromUri(uri);

                        




                        break;


                        case "WEATHER":
                        

                        // synth.SpeakAsync("What city would you like to know about?");
                        //rWeather = true;

                        //can ask for location and
                        //do a query to openweathermap
                        //synth.SpeakAsync("(response )");
                        string appid = "c9d2f62d46bdea0369e8bed9ec340ad7";
                        string city = "tonsberg";

                        //RecognitionResult result = speechEngine.Recognize(TimeSpan.FromSeconds(10));

//                        if (result != null)
  //                      {
    //                        Console.WriteLine(result.Text);
      //                  }


                        //string weburl = "http://api.openweathermap.org/data/2.5/weather?q=" + e.Result.Text + "&mode=xml";
                        string weburl = "http://api.openweathermap.org/data/2.5/weather?q=" + "tonsberg" + "&mode=xml" + "&APPID=" + appid;

                        var xml = new WebClient().DownloadString(new Uri(weburl));

                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(xml);

                        //extract weather data
                        string sTemp = xmlDoc.DocumentElement.SelectSingleNode("temperature").Attributes["value"].Value;
                        string sSkies = xmlDoc.DocumentElement.SelectSingleNode("clouds").Attributes["name"].Value;

                        //convert to celcius
                        //double temp = double.Parse(sTemp) - -272.15;
                        //double temp = Convert.ToDouble(sTemp);

                        double temp = double.Parse(sTemp, CultureInfo.InvariantCulture) - 272.15;
                        //double.Parse(sTemp);
                        //Convert.ToDateTime(sTemp);
                        //Convert.ToDouble(sTemp);

                        //string celcius = temp.ToString();


                        synth.Speak("the temperature of tonsberg is " + temp + "degrees celsius with a ." + sSkies);


                        break;

                        case "RWEATHER":
                            if(rWeather == true)
                        {
                            /*
                            //can ask for location and
                            //do a query to openweathermap
                            //synth.SpeakAsync("(response )");
                            string appid = "c9d2f62d46bdea0369e8bed9ec340ad7";
                            string city = "tonsberg";

                            RecognitionResult result = speechEngine.Recognize(TimeSpan.FromSeconds(10));

                            if (result != null)
                            {
                                Console.WriteLine(result.Text);
                            }


                            //string weburl = "http://api.openweathermap.org/data/2.5/weather?q=" + e.Result.Text + "&mode=xml";
                            string weburl = "http://api.openweathermap.org/data/2.5/weather?q=" + "tonsberg" + "&mode=xml" + "&APPID=" + appid;

                            var xml = new WebClient().DownloadString(new Uri(weburl));

                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(xml);

                            //extract weather data
                            string sTemp = xmlDoc.DocumentElement.SelectSingleNode("temperature").Attributes["value"].Value;
                            string sSkies = xmlDoc.DocumentElement.SelectSingleNode("clouds").Attributes["name"].Value;

                            //convert to celcius
                            double temp = double.Parse(sTemp) - -272.15;
                            //string celcius = temp.ToString();


                            synth.Speak("the temperature of tonsberg is " + temp.ToString() + "degrees celcius with a ." + sSkies);
                            */
                        }
                        break;

                        //information about the school
                        case "HSN":
                            synth.SpeakAsync("HSN is Norway's second largest state college, measured in number of students, and bigger than many of today's universities. HSN is currently led by principal Petter Aasen. We have approximately 17000 students and 1500 employees, and are spread across eight campuses ");
                            break;




                        //some fun 
                        case "JOKE":

                            //choose random joke
                            Random rand = new Random();
                            int joke = rand.Next(0, 11);
                            //Console.WriteLine(joke);
                            if (joke == 0)
                                synth.SpeakAsync("Why was the robot angry? Because someone kept pushing his buttons!");
                            else if (joke == 1)
                                synth.SpeakAsync("Why did the robot go back to robot school? Because his skills were getting a little rusty");
                            else if (joke == 2)
                                synth.SpeakAsync("What is a robot's favorite type of music? Heavy metal!!");
                            else if (joke == 3)
                                synth.SpeakAsync("What do you call an alligator in a vest? An in vesti gator");
                            else if (joke == 4)
                                synth.SpeakAsync("What did the picture go to jail? Because it was framed!");
                            else if (joke == 5)
                                synth.SpeakAsync("Why did the computer go to the doctor? Because it had a virus");
                            else if (joke == 6)
                                synth.SpeakAsync("How do crazy people go through the forest? They take the psycho path");
                            else if (joke == 7)
                                synth.SpeakAsync("Did you hear about the angry pancake? He just flipped!");
                            else if (joke == 8)
                                synth.SpeakAsync("What do prisoners use to call each other? Cell phones!");
                            else if (joke == 9)
                                synth.SpeakAsync("Did you hear about the guy who got hit in the head with a can of soda? He was lucky it was a soft drink!");
                            else if (joke == 10)
                                synth.SpeakAsync("Parallel lines have so much in common. Its a shame they will never meet.");
                            else if (joke == 11)
                                synth.SpeakAsync("What can you serve, but not eat? A volley-ball");

                            break;



                        //Cases for movement or mode switch

                        case "RIGHT":
                            //synth.
                            direction = "right";
                            break;

                        case "LEFT":

                            direction = "left";
                            break;
                        case "STOP":
                            direction = "x";
                            break;
                        case "CAMERA":
                            Mode.mode = "1";
                            break;
                        case "SPEECH":
                            Mode.mode = "3";
                            break;

                        case "REST":

                        USB.rest = true;

                            break;



                        default:
                            break;
                    }

                    //SHOULD BE TIMED SO YOU DONT HAVE TO ACTIVATE LISTENING EVERY TIME
                    //listening = false;

                }
                //If not already listening
                //else
                //{
                    /*
                    switch (e.Result.Semantics.Value.ToString())
                    {
                        case "ROTO":
                            switch (e.Result.Text)
                            {
                                case "hey":
                                    synth.SpeakAsync("Well hey there");
                                    break;
                                case "hello":
                                    synth.SpeakAsync("Well hello there");
                                    break;
                                case "roto":
                                    synth.SpeakAsync("i am roto");
                                    break;
                            }

                            //synth.SpeakAsync("hello! ");
                            listening = true;
                            break;
                    }
                } */
            // end if 

            else if (e.Result.Confidence > 0.45 && e.Result.Confidence < 0.6)
            {
            }
        } //END Speech Recognized
                /*
                synth.SpeakAsync("Did you ask: ");
                synth.SpeakAsync(e.Result.Text);
                cmd = e.Result.Text;
                //waiting for reply = true
                reply = true;

    */


            /*
                        else if (reply == true)
                        {
                            if (e.Result.Semantics.Value.ToString() == "yes")
                            {
                                speechEngine.SimulateRecognize(cmd);
                                reply = false;
                            }

                            System.Timers.Timer sw = new System.Timers.Timer();
                            sw.Enabled = true;
                            sw.Interval = 5000;
                            sw.Start();
                            sw.Elapsed += Sw_Elapsed;

                        }


                    }
                    */
            /*
            private void Sw_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
            {
                reply = false;
            } */

            /// <summary>
            /// Event fires when speechrecognition is rejected.
            /// </summary>
            /// <param name="sender">object sending the event</param>
            /// <param name="e">event arguments</param>
        private void SpeechEngine_SpeechRecognitionRejected(object sender, SpeechRecognitionRejectedEventArgs e)
        {
            //Console.WriteLine("¤!!");
            //throw new NotImplementedException();
        }
    }
}
